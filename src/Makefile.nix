ifdef CROSS_BUILD
CC         = x86_64-w64-mingw32-gcc
SDL_ROOT   = /usr/x86_64-w64-mingw32/sys-root/mingw
CFLAGS     += -I$(SDL_ROOT)/include
LDFLAGS    += -L$(SDL_ROOT)/lib -lmingw32 -lSDL2main -lSDL2

BIN_EXT    = .exe

else

SDL_CONFIG = $(shell command -v sdl2-config)

ifdef SDL_CONFIG
CFLAGS     += $(shell sdl2-config --cflags)
LDFLAGS    += $(shell sdl2-config --libs)

else
SDL_ROOT   = /usr
CFLAGS     += -I$(SDL_ROOT)/include/SDL2 -D_REENTRANT
LDFLAGS    += -L$(SDL_ROOT)/lib -pthread -lSDL2

BIN_EXT    =

endif # ifdef SDL_CHECK
endif # ifdef CROSS_BUILD

CP         = cp
RM         = rm -rfv
MKDIR      = mkdir -p
TRUE       = true

DIR_SEP    = /

ifdef CROSS_BUILD
copy-libs:
	$(CP) $(SDL_ROOT)/bin/SDL2.dll build

else
copy-libs:;

endif # ifdef CROSS_BUILD