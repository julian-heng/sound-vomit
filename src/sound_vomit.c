#include <SDL2/SDL.h>

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#if defined(__linux__)
#include <termios.h>
#include <sys/select.h>

#elif defined(_WIN32)
#include <conio.h>
#include <windows.h>

#endif


#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))


typedef struct
{
    char name[BUFSIZ];
    uint8_t* buffer;
    uint32_t length;
    SDL_AudioSpec spec;
} track_t;


typedef struct
{
    uint8_t* position;
    uint32_t length;
    uint32_t current_length;
    int volume;
} player_t;


typedef struct
{
    int argc;
    char** argv;

    player_t player;
    track_t current_track;
    bool is_initialised;
    bool exit;

#if defined(__linux__)
    struct termios term_default;
    struct termios term_current;
#endif
} program_t;


static bool prog_init(int argc, char** argv, program_t* prog);
static void prog_deinit(program_t* prog);
static void prog_main_loop(program_t* prog);

static bool prog_should_exit(program_t* prog);
static void prog_poll_events(program_t* prog);
static bool prog_input_available();
static void prog_print_status(program_t* prog);

static char* player_get_status_str(player_t* player);

static bool snd_load_wav(track_t* track, char fname[BUFSIZ]);
static void snd_play_callback(void* data, uint8_t* stream, int len);


int
main(int argc, char** argv)
{
    program_t prog = {
        .is_initialised = false,
    };

    if (! prog_init(argc, argv, &prog))
    {
        prog_deinit(&prog);
        return 1;
    }

    if (snd_load_wav(&prog.current_track, prog.argv[1]))
    {
        prog_main_loop(&prog);
    }

    prog_deinit(&prog);
    return 0;
}


static bool
prog_init(int argc, char** argv, program_t* prog)
{
    if (argc < 2)
    {
        return false;
    }

    if (SDL_Init(SDL_INIT_AUDIO) < 0)
    {
        return false;
    }

    prog->argc = argc;
    prog->argv = argv;

    prog->player.position = 0;
    prog->player.length = 0;
    prog->player.volume = 0;

#if defined(__linux__)
    tcgetattr(STDIN_FILENO, &prog->term_default);
    prog->term_current = prog->term_default;
    prog->term_current.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &prog->term_current);
#endif

    prog->is_initialised = true;

    return true;
}


static void
prog_deinit(program_t* prog)
{
    if (!prog->is_initialised)
    {
        return;
    }

#if defined(__linux__)
    tcsetattr(STDIN_FILENO, TCSANOW, &prog->term_default);
#endif

    SDL_CloseAudio();
    SDL_FreeWAV(prog->current_track.buffer);

    SDL_Quit();
}


static void
prog_main_loop(program_t* prog)
{
    SDL_AudioSpec spec = prog->current_track.spec;

    spec.callback = snd_play_callback;
    spec.userdata = prog;

    prog->player.position = prog->current_track.buffer;
    prog->player.length = prog->current_track.length;
    prog->player.current_length = prog->current_track.length;

    if (SDL_OpenAudio(&spec, NULL) < 0)
    {
        return;
    }

    SDL_PauseAudio(0);

    while (!prog_should_exit(prog))
    {
        prog_print_status(prog);
        SDL_Delay(16);

        prog_poll_events(prog);
    }

    printf("\n");
}


static bool
prog_should_exit(program_t* prog)
{
    return prog->exit || prog->player.current_length <= 0;
}


static void
prog_poll_events(program_t* prog)
{
    char in;

    if (!prog_input_available())
    {
        return;
    }

#if defined(__linux__)
    in = fgetc(stdin);
#elif defined(_WIN32)
    in = _getch();
#endif

    switch (in)
    {
        case 'q':
            prog->exit = true;
            break;

        case '+':
            prog->player.volume = MIN(prog->player.volume + 8, SDL_MIX_MAXVOLUME);
            break;

        case '-':
            prog->player.volume = MAX(prog->player.volume - 8, -128);
            break;
    }
}


static bool
prog_input_available()
{
#if defined(__linux__)
    struct timeval tv;

    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 100;

    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    select(STDIN_FILENO + 1, &fds, NULL, NULL, &tv);

    return (FD_ISSET(0, &fds));

#elif defined(_WIN32)
    return _kbhit();

#endif
}


static void
prog_print_status(program_t* prog)
{
    char* status_msg;
    status_msg = player_get_status_str(&prog->player);

#if defined(__linux__)
    printf("\r\033[2K%s", status_msg);

#elif defined(_WIN32)
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    int cols;

    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
    cols = csbi.srWindow.Right - csbi.srWindow.Left;

    char status_fmt[BUFSIZ];
    snprintf(status_fmt, BUFSIZ, "\r%%-%ds", cols);
        
    printf(status_fmt, status_msg);
#endif

    fflush(stdout);
}


static char*
player_get_status_str(player_t* player)
{
    static char buf[BUFSIZ];
    char* status_fmt = "%u / %u (%.2f%%) [vol: %d]";

    uint32_t current_pos = player->length - player->current_length;
    uint32_t length = player->length;
    float percent = length == 0 ? 0.0f : ((float)current_pos / length) * 100;
    int volume = player->volume;

    snprintf(buf, BUFSIZ, status_fmt, current_pos, length, percent, volume);
    return buf;
}


static bool
snd_load_wav(track_t* track, char fname[BUFSIZ])
{
    if (SDL_LoadWAV(fname, &track->spec, &track->buffer, &track->length) == NULL)
    {
        return false;
    }

    return true;
}


static void
snd_play_callback(void* data, uint8_t* stream, int len)
{
    program_t* prog = (program_t*)data;
    int current_length = prog->player.current_length;

    if (current_length == 0)
    {
        return;
    }

    len = MIN(len, current_length);
    SDL_memcpy(stream, prog->player.position, len);
    SDL_MixAudio(stream, prog->player.position, len, prog->player.volume);

    prog->player.position += len;
    prog->player.current_length -= len;
}
